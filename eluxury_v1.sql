--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.9
-- Dumped by pg_dump version 9.3.9
-- Started on 2015-12-04 21:39:02

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 2058 (class 1262 OID 16385)
-- Dependencies: 2057
-- Name: eluxury; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON DATABASE eluxury IS 'база для ярядом-лайт';


--
-- TOC entry 179 (class 3079 OID 11750)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2060 (class 0 OID 0)
-- Dependencies: 179
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 181 (class 3079 OID 16455)
-- Name: cube; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS cube WITH SCHEMA public;


--
-- TOC entry 2061 (class 0 OID 0)
-- Dependencies: 181
-- Name: EXTENSION cube; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION cube IS 'data type for multidimensional cubes';


--
-- TOC entry 180 (class 3079 OID 16527)
-- Name: earthdistance; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS earthdistance WITH SCHEMA public;


--
-- TOC entry 2062 (class 0 OID 0)
-- Dependencies: 180
-- Name: EXTENSION earthdistance; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION earthdistance IS 'calculate great-circle distances on the surface of the Earth';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 171 (class 1259 OID 16388)
-- Name: eluxe_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE eluxe_categories (
    id integer NOT NULL,
    name text,
    actual boolean,
    created time without time zone,
    order_number integer,
    image text
);


--
-- TOC entry 2063 (class 0 OID 0)
-- Dependencies: 171
-- Name: TABLE eluxe_categories; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE eluxe_categories IS 'Категории (рубрики) для поиска';


--
-- TOC entry 2064 (class 0 OID 0)
-- Dependencies: 171
-- Name: COLUMN eluxe_categories.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eluxe_categories.name IS 'название';


--
-- TOC entry 2065 (class 0 OID 0)
-- Dependencies: 171
-- Name: COLUMN eluxe_categories.actual; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eluxe_categories.actual IS 'актуальность';


--
-- TOC entry 2066 (class 0 OID 0)
-- Dependencies: 171
-- Name: COLUMN eluxe_categories.created; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eluxe_categories.created IS 'дата создания';


--
-- TOC entry 2067 (class 0 OID 0)
-- Dependencies: 171
-- Name: COLUMN eluxe_categories.order_number; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eluxe_categories.order_number IS 'порядковый номер';


--
-- TOC entry 170 (class 1259 OID 16386)
-- Name: eluxe_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE eluxe_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2068 (class 0 OID 0)
-- Dependencies: 170
-- Name: eluxe_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE eluxe_categories_id_seq OWNED BY eluxe_categories.id;


--
-- TOC entry 173 (class 1259 OID 16399)
-- Name: eluxe_orgs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE eluxe_orgs (
    id integer NOT NULL,
    title text,
    address text,
    lat double precision,
    lng double precision,
    phone text,
    url text,
    description text,
    actual boolean,
    created time without time zone,
    round_the_clock boolean,
    weight integer,
    address_extra text,
    img text
);


--
-- TOC entry 2069 (class 0 OID 0)
-- Dependencies: 173
-- Name: TABLE eluxe_orgs; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE eluxe_orgs IS 'Категории (рубрики) для поиска';


--
-- TOC entry 2070 (class 0 OID 0)
-- Dependencies: 173
-- Name: COLUMN eluxe_orgs.round_the_clock; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eluxe_orgs.round_the_clock IS 'круглосуточная';


--
-- TOC entry 2071 (class 0 OID 0)
-- Dependencies: 173
-- Name: COLUMN eluxe_orgs.address_extra; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eluxe_orgs.address_extra IS 'доп поле адреса';


--
-- TOC entry 174 (class 1259 OID 16410)
-- Name: eluxe_orgs_cat; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE eluxe_orgs_cat (
    cat_id integer NOT NULL,
    org_id integer NOT NULL
);


--
-- TOC entry 172 (class 1259 OID 16397)
-- Name: eluxe_orgs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE eluxe_orgs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2072 (class 0 OID 0)
-- Dependencies: 172
-- Name: eluxe_orgs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE eluxe_orgs_id_seq OWNED BY eluxe_orgs.id;


--
-- TOC entry 178 (class 1259 OID 16441)
-- Name: eluxe_orgs_offers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE eluxe_orgs_offers (
    id integer NOT NULL,
    description text,
    date_start time without time zone,
    date_end time without time zone,
    actual boolean,
    org_id integer,
    weight integer,
    time_start time without time zone,
    time_end time without time zone
);


--
-- TOC entry 2073 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN eluxe_orgs_offers.description; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eluxe_orgs_offers.description IS 'описание';


--
-- TOC entry 2074 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN eluxe_orgs_offers.date_start; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eluxe_orgs_offers.date_start IS 'дата начала';


--
-- TOC entry 2075 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN eluxe_orgs_offers.date_end; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eluxe_orgs_offers.date_end IS 'дата окончания';


--
-- TOC entry 2076 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN eluxe_orgs_offers.actual; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eluxe_orgs_offers.actual IS 'актуальность';


--
-- TOC entry 2077 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN eluxe_orgs_offers.time_start; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eluxe_orgs_offers.time_start IS 'Часы начала акции';


--
-- TOC entry 2078 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN eluxe_orgs_offers.time_end; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eluxe_orgs_offers.time_end IS 'Часы окончания акции';


--
-- TOC entry 177 (class 1259 OID 16439)
-- Name: eluxe_orgs_offers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE eluxe_orgs_offers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2079 (class 0 OID 0)
-- Dependencies: 177
-- Name: eluxe_orgs_offers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE eluxe_orgs_offers_id_seq OWNED BY eluxe_orgs_offers.id;


--
-- TOC entry 176 (class 1259 OID 16427)
-- Name: eluxe_orgs_wh; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE eluxe_orgs_wh (
    id integer NOT NULL,
    day integer,
    time_start time without time zone,
    time_end time without time zone,
    org_id integer
);


--
-- TOC entry 2080 (class 0 OID 0)
-- Dependencies: 176
-- Name: TABLE eluxe_orgs_wh; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE eluxe_orgs_wh IS 'рабочее время';


--
-- TOC entry 2081 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN eluxe_orgs_wh.day; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eluxe_orgs_wh.day IS 'день недели ';


--
-- TOC entry 2082 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN eluxe_orgs_wh.time_start; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eluxe_orgs_wh.time_start IS 'начальное время работы';


--
-- TOC entry 2083 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN eluxe_orgs_wh.time_end; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eluxe_orgs_wh.time_end IS 'конечное время работы';


--
-- TOC entry 175 (class 1259 OID 16425)
-- Name: eluxe_orgs_wh_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE eluxe_orgs_wh_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2084 (class 0 OID 0)
-- Dependencies: 175
-- Name: eluxe_orgs_wh_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE eluxe_orgs_wh_id_seq OWNED BY eluxe_orgs_wh.id;


--
-- TOC entry 1918 (class 2604 OID 16391)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY eluxe_categories ALTER COLUMN id SET DEFAULT nextval('eluxe_categories_id_seq'::regclass);


--
-- TOC entry 1919 (class 2604 OID 16402)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY eluxe_orgs ALTER COLUMN id SET DEFAULT nextval('eluxe_orgs_id_seq'::regclass);


--
-- TOC entry 1921 (class 2604 OID 16444)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY eluxe_orgs_offers ALTER COLUMN id SET DEFAULT nextval('eluxe_orgs_offers_id_seq'::regclass);


--
-- TOC entry 1920 (class 2604 OID 16430)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY eluxe_orgs_wh ALTER COLUMN id SET DEFAULT nextval('eluxe_orgs_wh_id_seq'::regclass);


--
-- TOC entry 2045 (class 0 OID 16388)
-- Dependencies: 171
-- Data for Name: eluxe_categories; Type: TABLE DATA; Schema: public; Owner: -
--

COPY eluxe_categories (id, name, actual, created, order_number, image) FROM stdin;
1	Туризм	t	00:27:48.806	0	tourism
2	Питание	t	19:24:01.437	1	food
3	Продукты	t	23:57:03.884	3	products
\.


--
-- TOC entry 2085 (class 0 OID 0)
-- Dependencies: 170
-- Name: eluxe_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('eluxe_categories_id_seq', 3, true);


--
-- TOC entry 2047 (class 0 OID 16399)
-- Dependencies: 173
-- Data for Name: eluxe_orgs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY eluxe_orgs (id, title, address, lat, lng, phone, url, description, actual, created, round_the_clock, weight, address_extra, img) FROM stdin;
7	Унция	Санкт-Петербург, пл. Сенная, 9	59.927238464399998	30.3172874451	5702373	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
8	Ням-Бург	Санкт-Петербург, пл. Сенная, 5	59.927589416499998	30.318096160900001	7166992	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
10	Subway	Санкт-Петербург, пл. Сенная, 3	59.927783966100002	30.318517684900002	5554449	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
11	Макдоналдс	Санкт-Петербург, ул. Ефимова, 1/4	59.9264335632	30.318994522099999	3259281	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
13	Крошка Картошка	Санкт-Петербург, пл. Сенная, 1	59.926429748499999	30.316968917800001	3000565	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
15	Метрополь	Санкт-Петербург, ул. Ефимова, 1/4	59.926300048800002	30.3192005157	3102440	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
75	Дикси	Санкт-Петербург, Московский просп., 10/12, литера М	59.924133300800001	30.319770813000002	3330201	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
81	Дикси	Санкт-Петербург, Столярный пер., 10/12, литера А, пом. 1-Н, 2-Н	59.927597045900001	30.3118743896	3330201	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
82	Театр Приют Комедианта	Санкт-Петербург, ул. Садовая, 27/9	59.929164886499997	30.323162078900001	3103314	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
5	Организация 2	Санкт-Петербург, Московский просп., 2/6, оф. 6а	59.925800323499999	30.318500518800001	7153522	\N	\N	t	15:55:27.135	f	5	\N	\N
1	Нотариус Иванова А.Н.	Санкт-Петербург, Спасский пер., 14/35, оф. 310	59.928131	30.3192005157	4571413	\N	\N	t	15:54:36.85	f	1	\N	thumb_01.jpg
2	Нотариус Полякова Т.И.	Санкт-Петербург, Московский просп., 2/6, оф. 6а	59.925800323499999	30.318500518800001	7153522	\N	\N	t	15:55:27.135	f	2	\N	thumb_02.jpg
3	Нотариальная контора	Санкт-Петербург, наб. канала Грибоедова, 56/58	59.9261779785	30.315715789799999	5712281	\N	\N	t	15:56:01.002	t	3	\N	thumb_03.jpg
4	Организация 1	Санкт-Петербург, Спасский пер., 14/35, оф. 310	59.928199768100001	30.3192005157	4571413	\N	\N	t	15:54:36.85	t	1	\N	thumb_04.jpg
16	Почтовое отделение № 31	Санкт-Петербург, пл. Сенная, 13/52	59.926860809300003	30.316352844200001	3107236	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
18	Петротур	Санкт-Петербург, Спасский пер., 14/35, БЦ На Сенной, оф. 410	59.928009033199999	30.319532394399999	6077730	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
19	Мастерская Художника	Санкт-Петербург, Гривцова пер., 22	59.927936553999999	30.316514968900002	6106434	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
21	Чебуречная Чебурритос	Санкт-Петербург, наб. канала Грибоедова, 52	59.9273872375	30.315832138099999	2887220	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
52	Диспетчерская служба Такси Адмиралтейское	Санкт-Петербург, ул. Гражданская, 5	59.928539276099997	30.314296722400002	3093954	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
54	Рубли	Санкт-Петербург, ул. Ефимова, 4	59.925636291499998	30.322237014799999	3825363	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
55	Арт Квартал	Санкт-Петербург, ул. Гражданская, 12	59.927509307900003	30.313280105600001	5710388	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
56	KillFish	Санкт-Петербург, ул. Садовая, 45	59.925449371299997	30.3144397736	3330977	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
20	Агентство недвижимости Перспектива Эстейт	Санкт-Петербург, Спасский пер., 14/35, БЦ На Сенной	59.928199768100001	30.3192005157	4570992	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
22	Контакт	Санкт-Петербург, ул. Садовая, 35	59.927890777599998	30.320198059100001	9985573	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
23	Магазин подарков СпбИгра	Санкт-Петербург, ул. Ефимова, 1, корп.4, БЦ Сенная 4, эт. 2	59.926067352300002	30.319728851299999	9341270	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
24	Хостел Маруся	Санкт-Петербург, ул. Ефимова, 1	59.925895690899999	30.319219589199999	9244234	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
25	Хот Рент	Санкт-Петербург, Московский просп., 2	59.925750732399997	30.318473815899999	9200565	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
26	Метрополь	Санкт-Петербург, Гривцова пер., 11	59.927932739299997	30.3159217834	4074345	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
28	Центральные Булочные	Санкт-Петербург, Гривцова пер., 20	59.928207397500003	30.3162994385	5702428	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
29	Великолукский мясокомбинат	Санкт-Петербург, ул. Садовая, 40	59.927341461200001	30.321168899500002	4382021	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
83	Столовая ложка	Санкт-Петербург, ул. Гражданская, 17	59.927875518800001	30.311889648400001	3153343	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
91	Музей Тропический Рай	Санкт-Петербург, Мучной пер., 3	59.930152893100001	30.321769714399998	3105847	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
92	Центральный музей железнодорожного транспорта РФ	Санкт-Петербург, ул. Садовая, 50	59.924293518100001	30.3137302399	3151476	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
93	Невские берега	Санкт-Петербург, Апраксин пер., 5	59.928134918200001	30.325202941899999	5707523	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
97	Столовая ложка	Санкт-Петербург, Антоненко пер., 10	59.929374694800003	30.312301635699999	3153401	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
99	Музей истории Русского географического общества	Санкт-Петербург, Гривцова пер., 10, литера А	59.930484771700002	30.314565658599999	3158335	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
101	Гостиница Фонтанка 99	Санкт-Петербург, наб. реки Фонтанки, 99	59.923542022699998	30.321571350100001	3104731	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
106	Петербургские отели	Санкт-Петербург, ул. Садовая, 25	59.929786682100001	30.324760436999998	3136154	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
32	Магазин Подарков Сувениров и Игрушек	Санкт-Петербург, наб. канала Грибоедова, 65	59.9267120361	30.314699173000001	3144052	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
36	Пироговый Дворик	Санкт-Петербург, ул. Ефимова, 3, ТК Сенная, этаж 1	59.925498962399999	30.320400238000001	6801620	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
41	У тещи на блинах	Санкт-Петербург, ул. Гороховая, 41/33	59.9287834167	30.320261001599999	3104405	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
42	Торговый Дом купца Яковлева	Санкт-Петербург, ул. Садовая, 38	59.928077697799999	30.3218326569	5703637	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
44	Гостиница Александрия	Санкт-Петербург, Спасский пер., 6/8	59.929183960000003	30.318979263300001	3108534	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
47	Кафе-бар Рамсес	Санкт-Петербург, Бринько пер., 6	59.925216674799998	30.3155536652	3107295	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
57	Симфония камня	Санкт-Петербург, ул. Садовая, 31	59.928707122799999	30.322092056300001	3209899	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
58	Капитал Отель	Санкт-Петербург, ул. Гороховая, 34	59.929073333700003	30.321365356400001	7165558	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
59	Мастерская Мастер Холст	Санкт-Петербург, Московский просп., 6	59.924484252900001	30.318355560299999	9532887	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
65	Дикси	Санкт-Петербург, ул. Гороховая, 32, литера А, пом. 8-Н	59.929485321000001	30.320835113499999	3330201	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
69	Бар-ресторан Моцарелла	Санкт-Петербург, наб. канала Грибоедова, 64	59.925422668499998	30.313379287699998	3106454	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
70	Центральные Булочные	Санкт-Петербург, ул. Гороховая, 29	59.930023193399997	30.319272994999999	3105121	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
71	Хостел Планета Питер	Санкт-Петербург, Московский просп., 8	59.924156189000001	30.319084167500002	9881088	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
72	Художественная мастерская Aurora - Art	Санкт-Петербург, ул. Казанская, 38	59.928749084499998	30.3132457733	5154748	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
74	Мини-гостиница Далиса	Санкт-Петербург, Московский просп., 10	59.924068450900002	30.318473815899999	7404780	NULL	Описание организации	t	21:56:29	f	1	NULL	NULL
\.


--
-- TOC entry 2048 (class 0 OID 16410)
-- Dependencies: 174
-- Data for Name: eluxe_orgs_cat; Type: TABLE DATA; Schema: public; Owner: -
--

COPY eluxe_orgs_cat (cat_id, org_id) FROM stdin;
1	1
1	2
1	3
2	4
2	5
3	7
2	8
2	10
2	11
2	13
1	19
1	20
2	21
2	22
1	23
1	24
1	25
2	26
3	28
2	28
3	29
1	32
2	36
2	41
3	42
1	44
2	47
1	52
2	54
1	55
2	56
1	57
1	58
1	59
3	65
2	69
3	70
1	71
1	72
1	74
3	75
3	81
1	82
2	83
1	91
1	92
2	93
2	97
1	99
1	101
1	106
\.


--
-- TOC entry 2086 (class 0 OID 0)
-- Dependencies: 172
-- Name: eluxe_orgs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('eluxe_orgs_id_seq', 107, true);


--
-- TOC entry 2052 (class 0 OID 16441)
-- Dependencies: 178
-- Data for Name: eluxe_orgs_offers; Type: TABLE DATA; Schema: public; Owner: -
--

COPY eluxe_orgs_offers (id, description, date_start, date_end, actual, org_id, weight, time_start, time_end) FROM stdin;
1	1111	\N	\N	t	1	1	\N	\N
2	22222	\N	\N	t	1	2	\N	\N
\.


--
-- TOC entry 2087 (class 0 OID 0)
-- Dependencies: 177
-- Name: eluxe_orgs_offers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('eluxe_orgs_offers_id_seq', 1, false);


--
-- TOC entry 2050 (class 0 OID 16427)
-- Dependencies: 176
-- Data for Name: eluxe_orgs_wh; Type: TABLE DATA; Schema: public; Owner: -
--

COPY eluxe_orgs_wh (id, day, time_start, time_end, org_id) FROM stdin;
1	1	01:00:00	09:00:00	1
2	1	10:00:00	23:30:00	1
\.


--
-- TOC entry 2088 (class 0 OID 0)
-- Dependencies: 175
-- Name: eluxe_orgs_wh_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('eluxe_orgs_wh_id_seq', 1, false);


--
-- TOC entry 1923 (class 2606 OID 16396)
-- Name: eluxe_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY eluxe_categories
    ADD CONSTRAINT eluxe_categories_pkey PRIMARY KEY (id);


--
-- TOC entry 1927 (class 2606 OID 16414)
-- Name: eluxe_orgs_cat_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY eluxe_orgs_cat
    ADD CONSTRAINT eluxe_orgs_cat_pkey PRIMARY KEY (cat_id, org_id);


--
-- TOC entry 1932 (class 2606 OID 16449)
-- Name: eluxe_orgs_offers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY eluxe_orgs_offers
    ADD CONSTRAINT eluxe_orgs_offers_pkey PRIMARY KEY (id);


--
-- TOC entry 1925 (class 2606 OID 16407)
-- Name: eluxe_orgs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY eluxe_orgs
    ADD CONSTRAINT eluxe_orgs_pkey PRIMARY KEY (id);


--
-- TOC entry 1929 (class 2606 OID 16432)
-- Name: eluxe_orgs_wh_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY eluxe_orgs_wh
    ADD CONSTRAINT eluxe_orgs_wh_pkey PRIMARY KEY (id);


--
-- TOC entry 1930 (class 1259 OID 16438)
-- Name: fki_eluxe_orgs_wh_org_fkey; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_eluxe_orgs_wh_org_fkey ON eluxe_orgs_wh USING btree (org_id);


--
-- TOC entry 1933 (class 2606 OID 16415)
-- Name: eluxe_orgs_cat_cat_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY eluxe_orgs_cat
    ADD CONSTRAINT eluxe_orgs_cat_cat_id_fkey FOREIGN KEY (cat_id) REFERENCES eluxe_categories(id);


--
-- TOC entry 1934 (class 2606 OID 16420)
-- Name: eluxe_orgs_cat_org_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY eluxe_orgs_cat
    ADD CONSTRAINT eluxe_orgs_cat_org_id_fkey FOREIGN KEY (org_id) REFERENCES eluxe_orgs(id);


--
-- TOC entry 1936 (class 2606 OID 16450)
-- Name: eluxe_orgs_offers_org_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY eluxe_orgs_offers
    ADD CONSTRAINT eluxe_orgs_offers_org_fkey FOREIGN KEY (org_id) REFERENCES eluxe_orgs(id);


--
-- TOC entry 1935 (class 2606 OID 16433)
-- Name: eluxe_orgs_wh_org_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY eluxe_orgs_wh
    ADD CONSTRAINT eluxe_orgs_wh_org_fkey FOREIGN KEY (org_id) REFERENCES eluxe_orgs(id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2015-12-04 21:39:06

--
-- PostgreSQL database dump complete
--

