select t.`title`, t.address, t.`phone_number`, t.lat, t.lng, null as url, 'Описание организации' as description, true as actual, now() as created, false as round_the_clock, 1 as weight, null as address_extra, null as img from (SELECT `title`, address, `phone_number`, lat, lng, eg.group_name,  (((acos(sin((59.9271*pi()/180)) * 
            sin((`lat`*pi()/180))+cos((59.9271*pi()/180)) * 
            cos((`lat`*pi()/180)) * cos(((30.3182- `lng`)* 
            pi()/180))))*180/pi())*60*1.1515
        ) as distance
        FROM orgparser_orgs org left join catalog_ya cy on org.cat_id=cy.yacat_id left join ourcatalog oc on cy.cat_id=oc.cat_id left join eluxe_groups eg on oc.group_id=eg.group_id WHERE org.city_id=14 AND eg.group_id in (2,4,6) AND address!='' and phone_number is not null group by concat(lat,lng)
        HAVING distance <= 0.3 order by distance asc) as t where 1
