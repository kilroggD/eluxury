<?php
/*
 * Константы
 */
require __DIR__.'/../defines.php';
require APP.'helpers.php';


$page = isset($_GET['page']) ? $_GET['page'] : 'main';
$debug = isset($_GET['debug']) ? $_GET['debug'] : 1;

include TEMPLATES.'index.php';