<?php
class Helpers {
  
	/**
	 * Антикеш - добавляем к файлу его версию
	 */
	public static function autoVer($url) 
  {
      $filename = PUBLIC_HTML.$url;
      
      if (file_exists($filename)) {
        $version = filemtime($filename);
        $url = $url.'?'.$version;
      }
      
      return BASE_URL.$url;
      //return '/'.$url;
	}

}