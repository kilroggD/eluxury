/*jslint browser: true, indent: 2, maxerr: 50, todo: true, white: true  */
/*global ga, $, jQuery, Helpers, PageMain */
var App = (function () {
  "use strict";
  
  var $body = $('body'),
    
    // Плагин карусели
    slider = function () {
    
      var $sliderList = $('#sliderList'),
        $nav = $('.js-nav'),
        $li = $('li', $sliderList),
        liH = $li.outerHeight(true),
        max = $li.length * liH * -1 + liH * 4;

      $nav.on('click', function () {
        var $this = $(this),
          dir = $this.data('dir'),
          y = parseInt($sliderList.css('top'), 10) || 0,
          margin = (dir === 'next' ? -liH : liH);
          
        y = y + margin * 4;

        $nav.removeClass('slider__nav--unactive');
          
        if (y > -liH) {
          y = 0;
          $nav.filter('[data-dir="prev"]').addClass('slider__nav--unactive');
          
        } else if (y < max + liH) {
          y = max;
          $nav.filter('[data-dir="next"]').addClass('slider__nav--unactive');
        }
        
        $sliderList.css('top', y);
        console.log(y);
      });
      
    },
    
    init = function () {
  
      slider();
  
    };

  return { init : init };
}());

$(function () {
  "use strict";
  App.init();
});