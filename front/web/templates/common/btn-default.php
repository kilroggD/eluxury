<?php
  // Шаблон кнопки с иконкой 
?>
<a href="#" class="btn--default <?php echo @$item->cls; ?>">
  <span class="btn__icon fg"><?php include PUBLIC_IMG_MIN.$item->iconFg.'.svg'; ?></span>
  <span class="btn__icon bg"><?php include PUBLIC_IMG_MIN.$item->iconBg.'.svg'; ?></span>
  <span class="btn__name <?php if (isset($item->nameBg)) : ?>fg<?php endif; ?>"><?php echo $item->nameFg; ?></span>
  <?php if (isset($item->nameBg)) : ?>
    <span class="btn__name bg"><?php echo $item->nameBg; ?></span>
  <?php endif; ?>
</a>
