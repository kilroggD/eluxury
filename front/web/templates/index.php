<!DOCTYPE html>
<!--[if lt IE 9]> <html lang="ru" class="ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ru"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Страница</title>
  <meta name="format-detection" content="telephone=no">
  <meta name="msapplication-tap-highlight" content="no">
  <?php /*  
  Иконки можно сгенерировать тут http://realfavicongenerator.net/
  */ ?>
  <link rel="stylesheet" href="<?php echo Helpers::autoVer('css/tmpl.css'); ?>" />
</head>
<body>
<div class="map__wrapper"></div>

<?php // Лого с кнопками ?>
<div class="info__wrapper">
  <a href="/" class="logo"><?php include PUBLIC_IMG_MIN.'logo.svg'; ?></a>
  
  <div class="h-row h-pr">    
    <?php 
      $CD = array();
      $i = 1;
      $CD[$i]->cls = 'h-fl';
      $CD[$i]->iconFg = 'rules';
      $CD[$i]->iconBg = 'rules_active';
      $CD[$i]->nameFg = 'Правила';
      
      $i++;
      $CD[$i]->cls = 'info__btn--mid';
      $CD[$i]->iconFg = 'flag';
      $CD[$i]->iconBg = 'flag_RU_active';
      $CD[$i]->nameFg = 'Выбор языка';
      $CD[$i]->nameBg = 'Русский';
      
      $i++;
      $CD[$i]->cls = 'h-fr';
      $CD[$i]->iconFg = 'info';
      $CD[$i]->iconBg = 'info_active';
      $CD[$i]->nameFg = 'Информация';
      
      foreach ($CD as $item) :
        include TEMPLATES_COMMON.'btn-default.php'; 
      endforeach;
    ?>
  </div>
  
  <div class="map__legend">
    <div class="map__legend__scale">100 метров</div>
    <div class="map__legend__desc">
      <div class="map__legend__icon"><?php include PUBLIC_IMG_MIN.'pin_1-1.svg'; ?></div>
      Открыто
    </div>
    <div class="map__legend__desc">
      <div class="map__legend__icon"><?php include PUBLIC_IMG_MIN.'pin_1-2.svg'; ?></div>
      Закрыто
    </div>
  </div>
</div>

<ul class="h-simple categories-list">
  <?php 
    $CD = array();
    $i = 1;    
    $CD[$i]->iconFg = 'tourism';
    $CD[$i]->iconBg = 'tourism_active';
    $CD[$i]->nameFg = 'Туризм';
    
    $i++;
    $CD[$i]->iconFg = 'food';
    $CD[$i]->iconBg = 'food_active';
    $CD[$i]->nameFg = 'Питание';
    
    $i++;
    $CD[$i]->iconFg = 'products';
    $CD[$i]->iconBg = 'products_active';
    $CD[$i]->nameFg = 'Продукты';
    
    $i++;
    $CD[$i]->iconFg = 'gift';
    $CD[$i]->iconBg = 'gift_active';
    $CD[$i]->nameFg = 'Подарки'; 

    $i++;
    $CD[$i]->iconFg = 'pharmacy';
    $CD[$i]->iconBg = 'pharmacy_active';
    $CD[$i]->nameFg = 'Аптеки'; 

    $i++;
    $CD[$i]->iconFg = 'transport';
    $CD[$i]->iconBg = 'transport_active';
    $CD[$i]->nameFg = 'Транспорт'; 

    $i++;
    $CD[$i]->iconFg = 'service';
    $CD[$i]->iconBg = 'service_active';
    $CD[$i]->nameFg = 'Услуги'; 

    $i++;
    $CD[$i]->iconFg = 'affiche';
    $CD[$i]->iconBg = 'affiche_active';
    $CD[$i]->nameFg = 'Афиша'; 

    $i++;
    $CD[$i]->iconFg = 'shopping';
    $CD[$i]->iconBg = 'shopping_active';
    $CD[$i]->nameFg = 'Торговые центры'; 

    $i++;
    $CD[$i]->iconFg = '24hours';
    $CD[$i]->iconBg = '24hours_active';
    $CD[$i]->nameFg = 'Круглосуточно'; 
    
    foreach ($CD as $item) :
  ?>
    <li class="category-item">
      <?php include TEMPLATES_COMMON.'btn-default.php'; ?>
    </li>
  <?php  
    endforeach;
  ?>
</ul>

<?php // Панель с адресами ?>
<aside class="address-panel">
  <div class="selected-place__img" style="background-image: url(<?php echo IMAGES_URL.'place_01.jpg'; ?>);"></div>
  <div class="selected-place__address">
    <div class="selected-place__pin"><?php include PUBLIC_IMG_MIN.'pib_b.svg'; ?></div>
    <div><strong>Невский пр. д. 18-20А</strong></div>
    <div>Название учреждения с терминалом</div>
  </div>
  <div class="address-panel__title">Столовые, кафе, рестораны, бары</div>
  
  <div class="slider__nav">
    <i class="slider__nav__icon icon-arr-t slider__nav--unactive js-nav" data-dir="prev"></i>
  </div>
  
  <div class="slider__wrapper" id="slider">
    <ul class="h-simple organisations__list" id="sliderList">
    <?php for ($s = 0; $s < 5; $s++) : ?>
      <?php for ($k = 1; $k < 5; $k++) : ?>
        <li class="organisation h-row">
          <div class="h-fl">
            <div class="organisation__name">Название учреждения, организации</div>
            <div class="organisation__info">
              <i class="organisation__info__icon icon-address"></i>
              Невский пр. д. 18-20А
            </div>
            <div class="organisation__info">
              <i class="organisation__info__icon icon-phone"></i>
              +7 564 565-56-56
            </div>
            <div class="organisation__info">
              <i class="organisation__info__icon icon-time"></i>
              10:00 - 19:00, без выходных
            </div>
            <div class="organisation__info">
              <i class="organisation__info__icon icon-site"></i>
              <a href="#">www.kuku.ru</a>
            </div>
            <div class="organisation__info">
              <i class="organisation__info__icon icon-info"></i>
              Информация: Несколько слов<br/> об объекте, услуги, сервис, еще<br/> что-нибудь интересное.
            </div>
          </div>
          <img src="<?php echo IMAGES_URL.'thumb_0'.$k.'.jpg'; ?>" class="organisation__info__img h-fr">
        </li>
      <?php endfor; ?>
    <?php endfor; ?>
    </ul>
  </div>
  
  <div class="slider__nav">
    <i class="slider__nav__icon icon-arr-b js-nav" data-dir="next"></i>
  </div>
</aside>
<!--[if lt IE 9]>
<noindex>
  <div class="old-browser">
    <div class="msg">
      <h3>Ваш браузер устарел!</h3>
      <p>Вы пользуетесь устаревшей версией браузера. Данная версия браузера не поддерживает многие современные технологии, из-за чего многие страницы могут отображаться некорректно, а главное — могут работать не все функции.</p>
      <p>В связи с этим предлагаем Вам установить более современный браузер из списка ниже. Все они бесплатны, легко устанавливаются и просты в использовании.</p>
      
      <ul class="simple browsers">
        <li><a href="http://www.google.com/chrome/" target="_blank" rel="noffolow" class="chrome"></a></li>
        <li><a href="http://www.mozilla.com/firefox/" target="_blank" rel="noffolow" class="firefox"></a></li>
        <li><a href="http://www.opera.com/download/" target="_blank" rel="noffolow" class="opera"></a></li>
        <li><a href="http://www.microsoft.com/windows/Internet-explorer/" target="_blank" rel="noffolow" class="msie"></a></li>
      </ul>
    </div>
  </div>
</noindex>
<![endif]-->
<!--[if gt IE 8]><!-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="build/jquery-2.1.4.min.js"><\/script>')</script>
<script src="<?php echo Helpers::autoVer('build/build.js'); ?>"></script>
<!--<![endif]-->
</body>
</html>