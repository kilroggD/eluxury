<?php
// Path
define('ROOT_PATH',		            __DIR__);
define('DS',		                  DIRECTORY_SEPARATOR);
define('APP',		                  ROOT_PATH.DS.'app'.DS);
define('WEB',		                  ROOT_PATH.DS.'web'.DS);
define('TEMPLATES',		            WEB.'templates'.DS);
define('TEMPLATES_PAGE',		      TEMPLATES.'pages'.DS);
define('TEMPLATES_COMMON',		    TEMPLATES.'common'.DS);
define('PUBLIC_HTML',		          ROOT_PATH.DS.'public_html'.DS);
define('PUBLIC_IMG',		          PUBLIC_HTML.DS.'img'.DS);
define('PUBLIC_IMG_MIN',		      PUBLIC_IMG.DS.'min'.DS);
define('PUBLIC_CSS',		          PUBLIC_HTML.DS.'css'.DS);

// Url
DEFINE('HTTP_ROOT', $_SERVER['HTTP_HOST']);
//DEFINE('HTTP_FOLDER', dirname($_SERVER['PHP_SELF']));
//DEFINE('HTTP_FOLDER', $_SERVER['REQUEST_URI']);
DEFINE('HTTP_FOLDER', str_replace("index.php", "", $_SERVER['SCRIPT_NAME']));
DEFINE('BASE_URL', "http://" . HTTP_ROOT . HTTP_FOLDER);
DEFINE('BASE_URL_FULL', "//" . HTTP_ROOT . HTTP_FOLDER);
DEFINE('IMAGES_URL', BASE_URL.'images/');
DEFINE('VIDEO_URL', BASE_URL.'video/');

//print_r($_SERVER);
//echo BASE_URL;