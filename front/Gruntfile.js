/*global module:false*/
module.exports = function(grunt) {
    "use strict";
    
    var api = grunt.file.readJSON('C:/Apache2.2/htdocs/tinypng_API.json'),
      imgFolder = 'public_html/img/',
      lessFolder = 'web/less/';
    
    grunt.initConfig({
      // https://github.com/marrone/grunt-tinypng
      tinypng: {
        options: {
            apiKey: api.key,
            checkSigs: true,
            sigFile: imgFolder + 'file_sigs.json',
            summarize: true,
            showProgress: true,
            stopOnImageError: true
        },
        compress: {
            src: ['*.png'],
            cwd: imgFolder,
            dest: imgFolder,
            expand: true
        }
      },
      
      // https://www.npmjs.com/package/grunt-run
      run: {
        csscomb: {
          cmd: 'task_css_comb.bat',
        },
        svgmin: {
          cmd: 'task_svg_min.bat',
        } 
      },
      
      // https://github.com/gruntjs/grunt-contrib-watch
      watch: {
        tinypng: {
          files: [imgFolder + '*.png'],
          tasks: ['tinypng'],
          options: {
            spawn: false,
            atBegin: true
          },
        },
        csscomb: {
          files: [lessFolder + '*.less', lessFolder + '**/*.less'],
          tasks: ['run:csscomb'],
          options: {
            spawn: false,
            atBegin: true,
            event: ['changed']
          },
        },
        svgmin: {
          files: [imgFolder + '*.svg'],
          tasks: ['run:svgmin'],
          options: {
            spawn: false,
            atBegin: true,
            event: ['added','changed']
          },
        },
      }
    });

    grunt.loadNpmTasks('grunt-tinypng');
    grunt.loadNpmTasks('grunt-contrib-watch');
    //grunt.loadNpmTasks('grunt-csscomb');
    grunt.loadNpmTasks('grunt-run');
    
    grunt.registerTask('default', ['tinypng']);
    //grunt.registerTask('png', ['tinypng']);
    //grunt.registerTask('css', ['csscomb:dist']);
    grunt.registerTask('r', ['run']);
};