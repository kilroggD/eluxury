###########################
#     ���������� 8)       #
###########################

� �������� ����������� ������������ grunt, grunt-watch, grunt-tinypng, grunt-run, csscomb.
��� ������� node � windows ��� (����� csscomb) ��������������� � ������� task_install_node_modules.bat.

������ ����������� task_watch.bat.

- grunt-tinypng (https://github.com/marrone/grunt-tinypng) ������� ���� png. ��� ������ ����� �������� ���������� API ���� (https://tinypng.com/developers).

- csscomb (https://github.com/csscomb/csscomb.js) ����������� less �����

- grunt-run (https://www.npmjs.com/package/grunt-run) ��������� task_css_comb.bat. Csscomb �� ���������� ��������� grunt`�� (������ ��� ������� ����� �����������), ������� ��������� ��� ����� .bat.

- grunt-watch (https://github.com/gruntjs/grunt-contrib-watch) ������� �� ���� � png � less �������, � �������� �������������� ����������




###########################
#           �����         #
###########################

�������, js, less � ����� /web

��� ���������� � ����� /public_html