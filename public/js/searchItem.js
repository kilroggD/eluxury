/* 
 * Темплейт организации для выдачи результатов поиска
 */
var searchItem='<li class="organisation h-row" id="<%=id %>">\n\
          <div class="h-fl">\n\
            <div class="organisation__name"><%=title %></div>\n\
            <div class="organisation__info"><i class="organisation__info__icon icon-address"></i><%=address %> (<%=address_extra %>)</div>\n\
            <div class="organisation__info"><i class="organisation__info__icon icon-phone"></i><%=phone %></div>\n\
            <div class="organisation__info"><i class="organisation__info__icon icon-time"></i>до закрытия: <%=time_to_close %></div>\n\
            <div class="organisation__info"><i class="organisation__info__icon icon-info"></i>Информация: <%=description %></div>\n\
          </div>\n\
          <img src="<?php echo IMAGES_URL.'thumb_0'.$k.'.jpg'; ?>" class="organisation__info__img h-fr">\n\
        </li>';

