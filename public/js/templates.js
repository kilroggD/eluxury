/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var Templates = {
  searchItem:'<li class="organisation h-row" data-org-id="<%=id %>">\n\
          <div class="h-fl">\n\
            <div class="organisation__name"><%=title %></div>\n\
            <div class="organisation__info organisation__address"><i class="organisation__info__icon icon-address"></i><%=address %> <% if(address_extra && address_extra!=="NULL"){print(" ("+address_extra+")")} else {print ("")} %></div>\n\
            <div class="organisation__info"><i class="organisation__info__icon icon-phone"></i><%=phone %></div>\n\
            <div class="organisation__info <% if(typeof(time_interval)!=="undefined") {if(time_interval<60 && time_interval>0) {print("org-timer")}} %>" data-role="org-time-to-close" data-interval="<% if(typeof time_interval !=="undefined"){ print(time_interval)} %>"><i class="organisation__info__icon icon-time"></i><span><% if(round_the_clock) { print(FrontConstants.labels.round_the_clock)} else {print(FrontConstants.labels.time_to_close+" "+time_to_close);} %></span></div>\n\
            <div class="organisation__info"><i class="organisation__info__icon icon-info"></i><% FrontConstants.information?print(FrontConstants.labels.information):print(""); %> <%=description %></div>\n\
          </div>\n\
          <img src="<% if(img && img!="NULL") {print("/images/"+img);} else {print("/images/no-image.jpg");} %>" class="organisation__info__img">\n\
        </li>'
    };