/* 
 *
 * Тестовая функция. эмулирует клики по задержке курсора на определенных элементах
 */
var Sight = function(options){
    this.cl=options["class"]?options["class"]:null;
    this.delay=options["delay"]?options["delay"]:null;
    return this.init();
    };

Sight.prototype = {
    init:function() {
        var self=this;   
        var sight;
        $("."+this.cl).on({
        mouseenter: function () {
        var $elem=$(this);
        sight = setTimeout(function(){
             $elem.click();
                 }, self.delay);
        },
        mouseleave: function () {
            clearTimeout(sight);            
       }
   });
    }
}

