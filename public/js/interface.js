﻿/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Апишки интерфейса
 * assuming global Map
 */
var Interface = {
    markers: [],
    timers:[],
    //листалка карусели
    carousel: null,
    init: function () {
        $(".slider__nav").hide();
        $("#sliderList").empty();
        //$("#dispatch-title").text("Пожалуйста, выберите категорию");
        this.mainEvents();
        this.extraEvents();
    },
    //основные события - поиск
    mainEvents: function () {
        var self = this;
        var links = $("a[data-role='search-link']"), slider = $('#slider');
        links.click(function (e) {
            e.preventDefault();
            self.dispatchLink($(this));
        });
        if (slider.length) {
            self.carousel = $('#slider').jcarousel({wrap:'circular'});
            $('.jcarousel-next').on('jcarouselcontrol:active', function () {
                $(this).removeClass('slider__nav--unactive');
            });
            $('.jcarousel-next').on('jcarouselcontrol:inactive', function () {
                $(this).addClass('slider__nav--unactive');
            });
            $('.jcarousel-prev').on('jcarouselcontrol:active', function () {
                $(this).removeClass('slider__nav--unactive');
            });
            $('.jcarousel-prev').on('jcarouselcontrol:inactive', function () {
                $(this).addClass('slider__nav--unactive');
            });
            $('.jcarousel-prev').jcarouselControl({
                target: '-=3'
            });

            $('.jcarousel-next').jcarouselControl({
                target: '+=3'
            });
            self.carousel.on("jcarousel:scrollend", function (event, carousel) {
                self.highlightMarkers(carousel)
            });
            self.carousel.on("jcarousel:reloadend", function (event, carousel) {
                //показываем слайдер
                 self.carousel.jcarousel('scroll', 0);
                if (!$(".slider__item").is(":visible") &&  $("#sliderList").children().length) {
                    $(".slider__item").show();
                }
                //показываем кнопки
                if (!$(".slider__nav").is(":visible") && $("#sliderList").children().length ) {
                    $(".slider__nav").show();
                }
                self.highlightMarkers(carousel);
            });
        }
    },
    //дополнительный события - версия для слабовидящих, языки и т д
    extraEvents: function () {
        var self=this;
        var llinks=$("a.locale-link");
        $(".btn-transit").hover(function(){$(this).find(".btn--default").addClass("hovered");},
        function(){$(this).find(".btn--default").removeClass("hovered");});
        $(".btn-transit").click(function(e){ e.preventDefault(); e.stopPropagation(); var $link=$(this).find(".btn--default"); self.dispatchLink($link); });
        llinks.click(function (e) {
            e.preventDefault();
            llinks.removeClass("active");
            $(this).addClass("active");
            var href = $(this).attr("href");
            window.location.href=href;
            return true;
        });
    },
    //применение таймеров обр. отсчета
    __appendTimers: function () {
        var $timers=$(".org-timer"),self=this;
        this.timers=[];
        $timers.each(function(){
           var minutes=$(this).attr("data-interval");           
           var $span=$(this).find("span");
           $span.empty();
           $span.countdown({until: '+' + minutes + 'm', format: 'MS', layout: "{desc}:&nbsp;&nbsp;{mnn}{sep}{snn}&nbsp;&nbsp;", description: FrontConstants.labels.time_to_close});
           self.timers.push($span);
        });
    },
    __destroyTimers:function(){
        var l=this.timers.length,i=0;
        for(i=0;i<l;++i) {
            this.timers[i].countdown('destroy');
        }
        this.timers=[];
    },
    //обработка ссылки
    dispatchLink: function ($link) {
        var self = this;
        var url = $link.attr("href"),title=$link.attr("title");
        $.get(url, function (response) {
            if (response.success) {
                var links = $("a[data-role='search-link']");
                links.removeClass("active");
                $link.addClass("active");
                if (response.orgs) {
                    var orgs = response.orgs;
                    if(typeof title!=='undefined') {
                        $("#dispatch-title").text(title).show();                        
                    }
                    $("#flaglist").hide();
                    self.__destroyTimers();
                    self.showMarkers(orgs);
                    self.__appendTimers();
                }
            }
            else {
                alert("Произошла ошибка");
            }
        }, "json");
    },
    //отображение маркеров-результатов поиска
    showMarkers: function (orgs) {
        var i, l = orgs.length, markers = [], marker, itemContent, panel = $("sliderList"), itemsFound = 0;
        if (this.markers.length) {
            //чистим маркеры, опустошаем панельку
            this.__clearMarkers();
            $("#sliderList").empty();
        }
        for (i = 1; i <= l; i++) {
            var icon, status, org = orgs[i - 1], popup;
            status = org["open"] ? "open" : "closed";
            icon = new L.Icon({
                iconUrl: '/img/markers/' + status + '.png',
                iconSize: [GlobalMapExtras.marker_org_size[0], GlobalMapExtras.marker_org_size[1]],
                popupAnchor: [3, -12], // point from which the popup should open relative to the iconAnchor
                iconAnchor: [3, 41]
            });
            itemContent = this.createOrgPanel(org);
            //если нашли контент - подвязываем к панельке
            if (itemContent) {
                $("#sliderList").append(itemContent);
                ++itemsFound;
            }
            marker = L.marker([org.lat, org.lng], {icon: icon, orgId: org.id, orgData: org, orgStatus: status});
            marker.addTo(Map);
            markers.push(marker);
        }
        this.markers = markers;
        if (itemsFound) {
            //перезагружаем слайдер            
            this.carousel.jcarousel('reload');
        }
        return markers;
    },
    createOrgPanel: function (org) {
        if(!org.img || org.img=='NULL'){
            org.img=_.shuffle(ImagesMock)[0];
        }
        var content = "";
        var orgTemplate = _.template(Templates.searchItem);
        //  console.log(orgTemplate);
        var content = orgTemplate(org);
        return content;
    },
    __clearMarkers: function () {
        var i, l = this.markers.length;
        for (i = 0; i < l; ++i) {
            Map.removeLayer(this.markers[i]);
        }
        this.markers = [];
    },
    //коллбэк для карусели. подсвечивает выбранные маркеры в рабочей области
    highlightMarkers: function (carousel) {
        //return true;
        var markers = this.markers, i, ids = [];
        if (markers) {
            var items = this.carousel.jcarousel('visible');
            for (i = 0; i < items.length; i++) {
                ids.push(parseInt($(items[i]).attr("data-org-id")));
            }
            for (i = 0; i < this.markers.length; i++) {
                var orgId = this.markers[i].options.orgId;
                if (ids.indexOf(orgId) !== -1) {
                    //console.log(this.markers[i]);
                    var newIcon = new L.Icon({
                        iconUrl: '/img/markers/' + this.markers[i].options.orgStatus + '_active'+'.png',
                        iconSize: [GlobalMapExtras.marker_org_active_size[0], GlobalMapExtras.marker_org_active_size[1]],
                        popupAnchor: [3, -12], // point from which the popup should open relative to the iconAnchor
                        iconAnchor: [4, 81]
                    });
                    this.markers[i].setIcon(newIcon);
                } else {
                    //console.log(this.markers[i]);
                    var newIcon = new L.Icon({
                        iconUrl: '/img/markers/' + this.markers[i].options.orgStatus + '.png',
                        iconSize: [GlobalMapExtras.marker_org_size[0], GlobalMapExtras.marker_org_size[1]],
                        popupAnchor: [3, -12], // point from which the popup should open relative to the iconAnchor
                        iconAnchor: [3, 41]
                    });
                    this.markers[i].setIcon(newIcon);
                }
            }
        }
    }
}

