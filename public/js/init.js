/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * глобальная инициализация карты
 */
var Map=null, FrontConstants={}, GlobalMapExtras={}, GlobalLang='ru_RU'; //глобальные переменные для того, что необходимо по всему проекту - карты, деньги, 2 ствола, лейблы, константы
var ImagesMock=["thumb_01.jpg","thumb_02.jpg","thumb_03.jpg","thumb_04.jpg"];
L.Map = L.Map.extend({
    openPopup: function(popup) {
        // this.closePopup(); 
        this._popup = popup;
        return this.addLayer(popup).fire('popupopen', {
            popup: this._popup
        });
    }
});
function initMap(options) {
                GlobalMapExtras=options.extra;
                var map = L.map('map',{controls: []});
                //30.2857 59.9161 30.3514 59.9362
                var southWest = L.latLng(options.bounds.swLat, options.bounds.swLng),
                        northEast = L.latLng(options.bounds.neLat, options.bounds.neLng),
                        bounds = L.latLngBounds(southWest, northEast);
                var weight=4.5;
                //add a tile layer to add to our map, in this case it's the 'standard' OpenStreetMap.org tile server
                L.tileLayer(options.url, {
                    attribution: '&copy; E-luxe',
                    maxZoom: options.zoom.max,
                    minZoom: options.zoom.min,
                    maxBounds: bounds
                }).addTo(map);

                map.attributionControl.setPrefix(''); // Don't show the 'Powered by Leaflet' text. Attribution overload

                var spb = new L.LatLng(options.center.lat, options.center.lng); // geographical point (longitude and latitude)
                map.setView(spb, options.zoom.min);
                map.fitBounds(bounds);
                map.dragging.disable();
                map.touchZoom.disable();
                map.doubleClickZoom.disable();
                map.scrollWheelZoom.disable();
                //маркер
                var mainIcon = new L.Icon({
                        iconUrl: '/img/markers/ya.png',
                        iconSize: [GlobalMapExtras.marker_ya_size[0], GlobalMapExtras.marker_ya_size[1]],
                        popupAnchor:  [6, -25], // point from which the popup should open relative to the iconAnchor
                        iconAnchor:   [12, 65]
                    });
                var marker = L.marker([options.center.lat, options.center.lng],{icon: mainIcon}).addTo(map);
            //    marker.bindPopup("<b>Вы ЗДЕСЬ</b>").openPopup();
                for (var r=options.center.radius;r>0;r-=100) {
                    var circle_options={
                    color: 'purple',
                    fillColor: '#9933ff',
                    fillOpacity: 0.0,
                    weight: weight
                }
                var circle = L.circle([options.center.lat, options.center.lng], r,circle_options).addTo(map);
                weight=1.5;
                }
            
               /* var circle = L.circle([options.center.lat, options.center.lng], options.center.radius, {
                    color: 'purple',
                    fillColor: '#9933ff',
                    fillOpacity: 0.0
                }).addTo(map);*/
                Map=map;
                return Map;
}

function initFrontConstants(constants) {
    FrontConstants=constants;
}

function initLang(lang) {
    GlobalLang=lang;
}
 
