<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return array(
    "labels" => array(
        "closed" => "Закрыто",
        "open" => "Открыто",
        "meters" => "метров",
        "unknown"=>"Неизвестно"
    ),
    "info" => array(
        "rules" => "Правила",
        "info" => "Информация",
        "flag" => "Выбор языка",      
    ),
    "search"=>array (
         "round_the_clock" => "Круглосуточно"
    )
    
);

