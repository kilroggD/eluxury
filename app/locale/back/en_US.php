<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    "labels" => array(
        "closed" => "Closed",
        "open" => "Open",
        "meters" => "meters",
        "unknown"=>"Unknown"
    ),
    "info" => array(
        "rules" => "Rules",
        "info" => "Info",
        "flag" => "Language",      
    ),
    "search"=>array (
         "round_the_clock" => "Round-the-clock"
    )
    
);