<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$router = new \Phalcon\Mvc\Router(false);
/*$router->add('/api/:action/:param/:value', [
    'controller' => 'api',
    'action' => 1,
    'param'=>2,
    'value'=>3
]);*/

$router->add("/init", ["controller"=>"index","action"=>"init"]);
$router->add(
    '/api/([a-z]+)[/]{0,1}([0-9]*)',
    array(
        'controller' => 'api',
        'action' => 'search',
        'param' => 1,      
        'value' => 2
    )
);
$router->add("/lang/([a-z_A-Z]+)",
        array(
            'controller'=>'index',
            'action'=>'language',
            'lang'=>1
        ));
$router->handle();
//die(var_dump($router));
return $router;
