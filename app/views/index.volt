<!DOCTYPE html>
<!--[if lt IE 9]> <html lang="{{lang_code}}" class="ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="{{lang_code}}"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>ЯРЯДОМ-Lite</title>
        <meta name="format-detection" content="telephone=no">
        <meta name="msapplication-tap-highlight" content="no">
        <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.5/leaflet.css" />
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/tmpl.css">
        <link rel="stylesheet" href="/css/jquery.countdown.css">
        <script src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/underscore-min.js"></script>
        <script src="js/jquery.jcarousel.min.js"></script>        
        <script src="js/jquery.plugin.min.js"></script>
        <script src="js/jquery.countdown.min.js"></script>
        <script src="js/leaflet.js"></script>        
        <script src="js/init.js"></script>
        <script src="js/templates.js"></script>
        <script src="js/interface.js"></script>
        <script src="js/sight.js"></script>
        <script>
            $(document).ready(function () {
                $.get("/init", function (response) {
                    if (response.success) {
                        initLang(response.lang);
                        initFrontConstants(response.front_constants);
                        initMap(response.map);
                        Interface.init();
                        var sight= new Sight(response.interface);
                    }
                    else {
                        alert("map initialization error!");
                    }
                },"json");
            });
        </script>
    </head>
    <body>
        {{ content() }}
    </body>
</html>
