<a href="#" class="btn--default interface-element" data-role="search-link">
    <?php $image=$item->image;  ?>
  <span class="btn__icon fg"><?php include "/../../public/img/min/".$image.".svg"; ?></span>
  <span class="btn__icon bg"><?php include "/../../public/img/min/".$image."_active.svg"; ?></span>
  <span class="btn__name">{{item.name}}</span>
</a>