<a href="{{item["url"]}}" class="btn--default {{item['class']}}" title="{{item['name']}}" data-role='{{item['data-role']}}'>    
    <?php $image=$item['img']; $imageA=$item['imgActive']; ?>
    <span class="btn__icon fg"><?php include "/../../public/img/min/".$image.".svg"; ?></span>
  <span class="btn__icon bg"><?php include "/../../public/img/min/".$imageA.".svg"; ?></span>
  <span class="btn__name">{{item['name']}}</span>
    <!--
  <span class="btn__icon fg"><?php include "/../../public/img/min/".$image.".svg"; ?></span>
  <span class="btn__icon bg"><?php include "/../../public/img/min/".$image."_active.svg"; ?></span>
  <span class="btn__name">{{item.name}}</span>-->
</a>