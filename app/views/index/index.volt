<div class="map__wrapper">
        <div class="map-container">
            <div id="map"></div>
        </div>
</div>
<div class="info__wrapper">
  <a href="/" class="logo interface-element"><img src="img/logo.png"/></a>    
    <div class="map__legend">
    <div class="map__legend__scale">100 {{labels['meters']}}</div>
    <div class="map__legend__desc">
      <div class="map__legend__icon">{% include "/../../public/img/min/pin_1-1.svg" %}</div>
      {{labels['open']}}
    </div>
    <div class="map__legend__desc">
      <div class="map__legend__icon">{% include "/../../public/img/min/pin_1-2.svg" %}</div>
      {{labels['closed']}}
    </div>
  </div>
    </div>
<?php // Панель с адресами ?>
<aside class="address-panel">
 <div class="selected-place__img" style="background-image: url('{{map['terminal']['img']}}');"></div>
  <div class="selected-place__address">
    <div class="selected-place__pin"><img src='{{map['terminal']['pin']}}' /></div>
    <div><strong>{{map['terminal']['address'][lang_code]}}</strong></div>
    <div>{{map['terminal']['name'][lang_code]}}</div>
  </div>

</aside>
<ul class="h-simple categories-list">
             {% for cat in cats %}
             {% set item=['id':cat.id,'name':cat.name,'img':cat.image,'imgActive':cat.image~'_active','url':url("api/cat/" ~  cat.id),'class':'','data-role':'search-link'] %}
                    <li class="category-item btn-transit interface-element" id='{{cat.id}}'>
      {{ partial("partials/categories", ['item':item ]) }}
    </li>
            {% endfor %}
            <!-- Псевдокатегория для поиска круглосуточных -->
            {% set rtc=['id':'rnd_search_id','name':rtc, 'img':"24hours",'imgActive':"24hours_active",'url':url("api/round"),'class':'','data-role':'search-link']  %}
                                <li class="category-item btn-transit interface-element" id='rnd_search_id'>
      {{ partial("partials/categories", ['item':rtc ]) }}
    </li>
</ul>



<div class="bbar">
      <div id="dispatch-title" class="address-panel__title">Пожалуйста, выберите удобный Вам язык</div>
            <ul class="flaglist two-line" id="flaglist"> 
      {% for locale,flag in flags %}
      <li><a href="{{url("?lang=" ~ locale)}}" id="{{locale}}" class="locale-link interface-element {% if (lang==locale) %} active {% endif %}"><img src="img/flags/{{flag}}.jpg" class="flag-image"/></a></li>
      {% endfor %}
      </ul>

      <div class="slider__wrapper">


  <div class="slider__item" id="slider">
    <ul class="h-simple organisations__list" id="sliderList">        
    </ul>
  </div>
  <div class="slider__nav jcarousel-prev nav__prev slider__item interface-element">
    <div class="slider__nav__icon_container">
   <!-- <i class="slider__nav__icon icon-arr-t  js-nav" data-dir="prev"></i> -->
    <img src="img/arrow2.png"/>
    </div>
  </div>
    <div class="slider__nav jcarousel-next nav__next slider__item interface-element">
    <div class="slider__nav__icon_container slider__nav">
    <!--<i class="slider__nav__icon icon-arr-b js-nav" data-dir="next"></i>-->
    <img src="img/arrow1.png"/>
    </div>
  </div>
</div>
</div>
<div class="bbar2">
<div id="current-language" class="language-block">
<a href="{{url()}}" class="locale-link interface-element"><img src="img/flags/{{flags[lang]}}.jpg" class="flag-image"></a>
</div>
</div>
