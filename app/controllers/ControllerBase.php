<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    protected static $defaultLang="ru_RU";
    protected static $langs=array(
        "ru_RU"=>"Russia",
        "en_US"=>"United_Kingdom",
        "de_DE"=>"Germany",
        "fr_FR"=>"France",
        "fi_FI"=>"Finland",
        "cn_CN"=>"China",
        "it_IT"=>"Italy",
        "sp_SP"=>"Spain"
    );
    public function initialize() {
      if(!$this->getLang()) {      
          $this->setLang(self::$defaultLang);
      }      
    }
    
    public function getTranslation($key,$section=null) {
        $locale= require APP_PATH . "/app/locale/back/".$this->session->get("lang").".php";
        if($section) {
            $locale=$locale[$section];
        }
        if(isset($locale[$key])) {
            return $locale[$key];
        } else {
            return $key;
        }
    }
    //возвращает секцию со значениями
    public function getLocaleSection($section) {
        $locale= require APP_PATH . "/app/locale/back/".$this->session->get("lang").".php";
        if($section) {
            return $locale[$section];
        }
        return array();
    }
    
    public function getLang(){
        $locale=$this->session->get("lang");
        return  $locale;
    }
    //код языка (например для js)
    public function getLangCode(){
        $locale=$this->session->get("lang");
        $codes=  explode("_", $locale);
        return  $codes[0];
    }
    
    public function setLang($lang) {
        //если языка нет в локалях
        if(!isset(self::$langs[$lang])) {
            $lang=self::$defaultLang;
        } 
        elseif(!file_exists(APP_PATH . "/app/locale/back/".$lang.".php")) {
        //костыль если нет перевода
            $lang="en_US";
        }
        $this->session->set("lang", $lang);      
        return $lang;
    }
    
}
