<?php

class IndexController extends ControllerBase {

    public function indexAction() {
        $request = new \Phalcon\Http\Request();
        $lang=$request->get("lang");
        if($lang) {
            $this->setLang($lang);
        }
        $cats = new EluxeCategories();
        $catsFound = $cats->find(array(
            "actual = true",
            "order" => "order_number"
        ));
        $result=$this->translateCats($catsFound);
        $this->view->cats=$result;
        $infoKeys = array("rules","flag","info");
        //массив инфоссылок
        $info = $this->getLocaleSection("info");
        $search = $this->getLocaleSection("search");
        $this->view->setVar("info", $this->prepareMenu($info));
        $this->view->setVar("rtc",$search['round_the_clock']) ;
        //язык в том числе для флагов
        $this->view->setVar("lang", $this->getLang());
        $this->view->setVar("lang_code",$this->getLangCode());
        //лейблы - откр/закр и т д
        $this->view->setVar("labels", $this->getLocaleSection("labels"));
        $this->view->setVar('map', $this->config["map"]);
        $this->view->setVar('flags',self::$langs);
        //лейблы для скриптов, фронтенда и т д
        // var_dump($this->config);die;
    }

    public function initAction() {
        $this->view->disable();
        $result["map"] = $this->config["map"];
        $result["interface"] = $this->config["interface"];
        $result["map"]["lang"]=$this->getLangCode();        
        if ($result) {
            $result["success"] = true;
            $frontLocale = require APP_PATH . "/app/locale/front/" . $this->session->get("lang") . ".php";
            $result["front_constants"] = $frontLocale;
        }
        $response = new \Phalcon\Http\Response();
        $response->setContentType('application/json', 'UTF-8');
        $response->setContent(json_encode($result));
        return $response;
    }

    protected function prepareMenu($info) {
        $menu = array();
        $classes=array('h-fl','info__btn--mid','h-fr');
        $k=0;
        $lang=$this->getLang();
        foreach ($info as $key=>$item) {            
            $link["name"]=$item;
            $link["img"]=$key;
            $link["imgActive"]=$key!='flag'?$key."_active":$key."_".$lang."_active";
            $link["url"]="#".$key."-window";
            $link["class"]=isset($classes[$k])?$classes[$k]:'';            
            $menu[]=$link;
            $k++;
        }
        return $menu;
    }
    
    protected function translateCats($catsFound) {
        $result=array();
        $lang=$this->getLang();
        if($lang!=self::$defaultLang && isset(self::$langs[$lang])) {
            foreach($catsFound as $index=>$cat) {
                $translations = $cat->getTranslations("lang_code='$lang'");
                $translation=$translations->getFirst();
                $trName=$translation->cat_name;
                $cat->name=$trName;
                $result[$index]=$cat;
            }
        } else {
            $result=$catsFound;
        }
        return $result;
    }

}
