<?php

use Phalcon\Mvc\Model\Resultset;

class ApiController extends ControllerBase {

    public function indexAction() {
        
    }

    public function searchAction() {
        $this->view->disable();
        $request = new Phalcon\Http\Request();
        // if($request->isAjax()) {
        if (1) {
            //параметры для поиска                    
            $param = $this->dispatcher->getParam("param");
            $value = $this->dispatcher->getParam("value");
            $success = true;
            switch ($param) {
                //поиск по категории
                case "cat":
                    if (!$value) {
                        $success = false;
                        $result = array("success" => false, "message" => "Не задана рубрика для поиска");
                        break;
                    }
                    $res = $this->searchByCat($value);
                    /* $mdl=new EluxeOrgs();
                      $result = $mdl->searchByCat($value); */
                    break;
                //поиск круглосуточных
                case "round":
                    $res = EluxeOrgs::searchByRound();
                    break;
                //поиск объектов с акциями
                case "offers":
                    $res = $this->searchWithOffers();
                    break;
                default:
                    $success = false;
                    $result = array("success" => false, "message" => "Не задан параметр поиска");
                    break;
            }
            if ($success) {
                $result["orgs"] = $this->prepareOrgs($res);
                $result["success"] = true;
            }
            $response = new \Phalcon\Http\Response();
            $response->setContentType('application/json', 'UTF-8');
            $response->setContent(json_encode($result));
            return $response;
        } else {
            throw new Exception("Прямой вызов метода запрещен");
        }
    }

    //поиск по категории
    protected function searchByCat($cat) {
        $phql = "SELECT " . EluxeOrgs::MAIN_COLS . " FROM EluxeOrgs JOIN EluxeOrgsCat ON EluxeOrgs.id=EluxeOrgsCat.org_id WHERE  EluxeOrgsCat.cat_id=:cat: and EluxeOrgs.actual=true ORDER BY EluxeOrgs.weight ASC";
        $query = $this->modelsManager->createQuery($phql);
        return $query->execute(array("cat" => $cat))->toArray();
    }

    //поиск объектов с акциями
    protected function searchWithOffers() {
        $result = array();
        $phql = "SELECT " . EluxeOrgs::MAIN_COLS . " from EluxeOrgs LEFT JOIN EluxeOrgsOffers ON EluxeOrgsOffers.org_id=EluxeOrgs.id where EluxeOrgs.actual=true and EluxeOrgsOffers.actual=true GROUP BY EluxeOrgs.id ORDER BY EluxeOrgs.weight ASC";
        $query = $this->modelsManager->createQuery($phql);
        $result = $query->execute()->toArray();
        return $result;
    }

    protected function prepareOrgs($orgs) {
        $result = array();
        if ($orgs) {
            $today = new DateTime();
            $dow = $today->format("N");
            $time = $today->format("H:i");
            foreach ($orgs as $org) {
                $cats = $this->modelsManager->executeQuery("SELECT EluxeCategories.id,EluxeCategories.name from EluxeCategories left join EluxeOrgsCat on EluxeCategories.id=EluxeOrgsCat.cat_id where EluxeCategories.actual=true and EluxeOrgsCat.org_id=:org_id: ORDER BY EluxeCategories.order_number ASC", array("org_id" => $org["id"]))->toArray();
                $offers = EluxeOrgsOffers::find(array("conditions" => "org_id=:org_id: AND actual=true", "bind" => array("org_id" => $org["id"]), "order" => "weight ASC"));
                $org["categories"] = $cats;
                $org["offers"] = array();
                $offersArray = $offers->toArray();
                foreach ($offersArray as $off) {
                    if ($off["time_start"] && $off["time_end"] && $off["time_start"] < $time && $time < $off["time_end"]) {
                        $off["on_time"] = 1;
                        $dtEnd = new \DateTime($off["time_end"]);
                        $diff = $dtEnd->diff($today);
                        $off["before_end"] = $diff->format('%H:%I');
                    } else {
                        $off["on_time"] = 0;
                    }
                    $org["offers"][] = $off;
                }
                if (!$org["round_the_clock"]) {
                    //проверяем есть ли вообще рабочее время
                    $hasWh = EluxeOrgsWh::findFirst(array("columns" => "EluxeOrgsWh.time_start,EluxeOrgsWh.time_end", "conditions" => "org_id=:org_id:", "bind" => array("org_id" => $org["id"])));
                    if ($hasWh) {
                        $wh = EluxeOrgsWh::findFirst(array("columns" => "EluxeOrgsWh.time_start,EluxeOrgsWh.time_end", "conditions" => "org_id=:org_id: AND day=$dow AND time_start<'$time'", "bind" => array("org_id" => $org["id"]), "order" => "time_end DESC"));
                        if ($wh) {
                            $org["worktime"] = $wh->toArray();
                            if ($org["worktime"]["time_start"] < $time && $time < $org["worktime"]["time_end"]) {
                                $minutes = 0;
                                $dtEnd = new \DateTime($org["worktime"]["time_end"]);
                                $diff = $dtEnd->diff($today);
                                $org["time_to_close"] = $diff->format('%H:%I');
                                $minutes += $diff->h * 60;
                                $minutes += $diff->i;
                                $org["time_interval"] = $minutes;
                                $org["open"] = 1;
                            } else {
                                $org["time_to_close"] = $this->getTranslation("closed", "labels");
                                $org["time_interval"] = 0;
                                $org["open"] = 0;
                            }
                        } else {
                            $org["time_to_close"] = $this->getTranslation("closed", "labels");
                            $org["time_interval"] = 0;
                            $org["worktime"] = false;
                            $org["open"] = 0;
                        }
                    } else {
                        $org["time_to_close"] = $this->getTranslation("unknown", "labels");
                        $org["time_interval"] = 0;
                        $org["worktime"] = false;
                        $org["open"] = 0;
                    }
                } else {
                    $org["open"] = 1;
                }
                $result[] = $org;
            }
        }
        return $result;
    }

}
