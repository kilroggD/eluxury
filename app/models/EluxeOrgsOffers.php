<?php

class EluxeOrgsOffers extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $date_start;

    /**
     *
     * @var string
     */
    public $date_end;

    
    /**
     *
     * @var string
     */
    public $time_start;

    /**
     *
     * @var string
     */
    public $time_end;

    
    
    /**
     *
     * @var string
     */
    public $actual;

    /**
     *
     * @var integer
     */
    public $org_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('org_id', 'EluxeOrgs', 'id', array('alias' => 'EluxeOrgs'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'eluxe_orgs_offers';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return EluxeOrgsOffers[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return EluxeOrgsOffers
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function searchByOrg($orgId) {

    }
}
