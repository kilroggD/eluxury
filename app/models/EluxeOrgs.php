<?php

use Phalcon\Mvc\Model\Query;

class EluxeOrgs extends \Phalcon\Mvc\Model {
    const MAIN_COLS="EluxeOrgs.id,EluxeOrgs.title,EluxeOrgs.address,EluxeOrgs.address_extra,EluxeOrgs.phone,EluxeOrgs.description,EluxeOrgs.round_the_clock, EluxeOrgs.lat, EluxeOrgs.lng, EluxeOrgs.img";
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $address;

    /**
     *
     * @var string
     */
    public $lat;

    /**
     *
     * @var string
     */
    public $lng;

    /**
     *
     * @var string
     */
    public $phone;

    /**
     *
     * @var string
     */
    public $url;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $actual;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *  @var boolean
     */
    public $round_the_clock;

        /**
     *
     * @var string
     */
    public $address_extra;
    
    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSchema("public");
        $this->hasMany('id', 'EluxeOrgsCat', 'org_id', array('alias' => 'eluxeOrgsCat'));
        $this->hasMany('id', 'EluxeOrgsWh', 'org_id', array('alias' => 'eluxeOrgsWh'));
        $this->hasMany('id', 'EluxeOrgsOffers', 'org_id', array('alias' => 'eluxeOrgsOffers'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'eluxe_orgs';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return EluxeOrgs[]
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return EluxeOrgs
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    //поиск круглосуточных
    public static function searchByRound() {
        $orgs = self::find(array("columns"=>self::MAIN_COLS, "conditions" => "round_the_clock=true and actual=true", "order" => "weight", "limit" => 20));
        return $orgs->toArray();
    }

    //поиск объектов с акциями
    public static function searchByIds($ids) {            
        $orgs = self::find(array("columns"=>self::MAIN_COLS, "conditions" => "id IN ({ids:array})", "bind"=>array('ids'=>$ids), "order" => "weight", "limit" => 20));
        return $orgs->toArray();    
    }
        

}
