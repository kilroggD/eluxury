<?php

class EluxeCategories extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $actual;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var integer
     */
    public $order_number;

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSchema("public");
        $this->hasMany('id', 'EluxeOrgsCat', 'cat_id', array('alias' => 'EluxeOrgsCat'));
        $this->hasMany('id', 'EluxeCategoriesTr', 'cat_id', array('alias' => 'EluxeCategoriesTr'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'eluxe_categories';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return EluxeCategories[]
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return EluxeCategories
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }
    
        /**
     * Возвращает соответствующие локализации
     *
     * @return \RobotsParts[]
     */
    public function getTranslations($parameters = null)
    {
        return $this->getRelated('EluxeCategoriesTr', $parameters);
    }

}
