<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EluxeCategoriesTr
 *
 * @author user4
 */
class EluxeCategoriesTr extends \Phalcon\Mvc\Model{
    //put your code here
        /**
     *
     * @var integer
     */
    public $cat_id;

    /**
     *
     * @var string
     */
    public $lang_code;
    
    /**
     *
     * @var string 
     */
    public $cat_name;
    
        public function initialize()
    {
        $this->setSchema("public");
        $this->setSource('eluxe_categories_tr');
        $this->belongsTo('cat_id', 'EluxeCategories', 'id', array('alias' => 'EluxeCategories'));
    }
    
        public function getSource()
    {
        return 'eluxe_categories_tr';
    }
    
    
}
