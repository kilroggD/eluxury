<?php

class EluxeOrgsCat extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cat_id;

    /**
     *
     * @var integer
     */
    public $org_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
        $this->belongsTo('cat_id', 'EluxeCategories', 'id', array('alias' => 'EluxeCategories'));
        $this->belongsTo('org_id', 'EluxeOrgs', 'id', array('alias' => 'EluxeOrgs'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'eluxe_orgs_cat';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return EluxeOrgsCat[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return EluxeOrgsCat
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
